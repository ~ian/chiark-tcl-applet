
# Copyright 2016,2020 Ian Jackson
# SPDX-License-Identifier: GPL-3.0-or-later
# There is NO WARRANTY.

proc manyset {list args} {
    foreach val $list var $args {
        upvar 1 $var my
        set my $val
    }
}

namespace eval debug {

# Debug:
# 
#    Caller may call
#       debug::setup ON-DEBUG
#    which will result in calls to [concat ON-DEBUG [list MESSAGE]]
#    (or ON-DEBUG may be "" in which case messages are discarded)
#
#    And caller may call
#       debug::debug MSG

variable debug {}

proc debug {m} {
    variable debug
    if {![llength $debug]} return
    set ns [uplevel 1 namespace current]
    uplevel #0 $debug [list "DEBUG $ns $m"]
}

proc setup {d} {
    variable debug $d
}

}
