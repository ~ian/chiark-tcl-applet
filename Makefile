
# Copyright 2016,2020 Ian Jackson
# SPDX-License-Identifier: GPL-3.0-or-later
# There is NO WARRANTY.

INSTALL ?= install
INSTALL_SCRIPT ?= $(INSTALL) -m 755
INSTALL_DATA ?= $(INSTALL) -m 644

prefix ?= /usr/local

p=chiark-tcl-applet

bindir=$(prefix)/bin
sharedir=$(prefix)/share/$p

SCRIPTS += xbatmon-simple-tray chiark-tcl-applet-wrapper
TCLLIBS += $(addsuffix .tcl, applet args subproc utils)

SEDDED_SCRIPTS= $(addprefix tmp/, $(SCRIPTS))

all:	$(SEDDED_SCRIPTS)

tmp:
	mkdir -p tmp

tmp/%:	% tmp
	sed '1,/^source/ s#^set lib \.#set lib $(sharedir)#' $* >$@
	chmod +x $@

install: all
	$(INSTALL) -d $(addprefix $(DESTDIR), $(bindir) $(sharedir))
	$(INSTALL_SCRIPT) $(SEDDED_SCRIPTS) $(DESTDIR)$(bindir)
	$(INSTALL_DATA) $(TCLLIBS) $(DESTDIR)$(sharedir)

clean:
	rm -rf tmp
