
# Copyright 2016,2020 Ian Jackson
# SPDX-License-Identifier: GPL-3.0-or-later
# There is NO WARRANTY.

namespace eval args {

proc badusage {msg} {
    puts stderr "bad usage: $msg"
    exit 12
}

proc badoption {} {
    variable lastarg
    badusage "unknown option $lastarg"
}

proc next {} {
    global argv
    variable lastarg
    if {![llength $argv]} { badusage "$lastarg needs a value" }
    set arg [lindex $argv 0]
    set argv [lrange $argv 1 end]
    set lastarg $arg
}

proc next_num {} {
    set arg [next]
    variable lastarg
    if {[catch { eval {$arg + 0} } emsg]} { 
	badusage "$lastargv value must be a number ($emsg)"
    }
}

proc generalarg {arg} {
    switch -exact -- $arg {
	-width { set applet::w [next_num] }
	-height { set applet::h [next_num] }
	-horizontal - -vertical { set applet::deforient $arg }
	-borderColour - -borderColor { set applet::border_colour [next] }
	-borderWidth { set applet::border_width [next_num] }
	-debug { debug::setup puts }
	default { return 0 }
    }
    return 1
}

proc more {} {
    global argv
    if {![llength $argv]} { return 0 }
    if {![regexp {^-} [lindex $argv 0]]} { return 0 }
    return 1
}

proc next_special {va} {
    upvar 1 $va arg
    global argv
    while {[more]} {
	set arg [next]
	if {[generalarg $arg]} continue
	return 1
    }
    return 0
}

}
